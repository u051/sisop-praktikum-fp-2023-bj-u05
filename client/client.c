#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>

#define PORT 8080

void send_command(int sock, const char *command, const char *argument) {
    send(sock, command, strlen(command), 0);
    usleep(100000);
    send(sock, argument, strlen(argument), 0);
}

int main(int argc, char *argv[]) {
    int sock = 0;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\nSocket creation error\n");
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported\n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed\n");
        return -1;
    }

    char username[256];
    char password[256];

    printf("Username: ");
    fgets(username, sizeof(username), stdin);
    username[strcspn(username, "\n")] = '\0';

    printf("Password: ");
    fgets(password, sizeof(password), stdin);
    password[strcspn(password, "\n")] = '\0';

    send_command(sock, username, password);

    char response[256];
    memset(response, 0, sizeof(response));
    read(sock, response, sizeof(response));
    printf("Server response: %s\n", response);

    if (strcmp(response, "Authentication successful.") == 0) {
        while (1) {
            printf("\nAvailable commands:\n");
            printf("1. CREATE DATABASE\n");
            printf("2. GRANT PERMISSION\n");
            printf("3. USE DATABASE\n");
            printf("4. EXIT\n\n");
            printf("Enter command number: ");

            int choice;
            scanf("%d", &choice);
            getchar(); // Clear the newline character from the input buffer

            char argument[256];
            memset(argument, 0, sizeof(argument));

            switch (choice) {
                case 1:
                    printf("Enter database name: ");
                    fgets(argument, sizeof(argument), stdin);
                    argument[strcspn(argument, "\n")] = '\0';
                    send_command(sock, "CREATE DATABASE", argument);
                    break;
                case 2:
                    printf("Enter database name: ");
                    fgets(argument, sizeof(argument), stdin);
                    argument[strcspn(argument, "\n")] = '\0';
                    send_command(sock, "GRANT PERMISSION", argument);
                    break;
                case 3:
                    printf("Enter database name: ");
                    fgets(argument, sizeof(argument), stdin);
                    argument[strcspn(argument, "\n")] = '\0';
                    send_command(sock, "USE DATABASE", argument);

                    memset(response, 0, sizeof(response));
                    read(sock, response, sizeof(response));
                    printf("Server response: %s\n", response);
                    break;
                case 4:
                    send_command(sock, "EXIT", "");
                    printf("Exiting...\n");
                    close(sock);
                    return 0;
                default:
                    printf("Invalid choice. Please try again.\n");
            }
        }
    }

    close(sock);
    return 0;
}
