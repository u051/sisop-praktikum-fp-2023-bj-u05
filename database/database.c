#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>

#define PORT 8080
#define MAX_DATABASES 10
#define MAX_DATABASE_NAME_LENGTH 256
#define MAX_TABLE_NAME_LENGTH 256

typedef struct {
    char name[MAX_DATABASE_NAME_LENGTH];
    int permission;
} Database;

typedef struct {
    char name[MAX_TABLE_NAME_LENGTH];
} Table;

typedef struct {
    char username[256];
    char password[256];
    int root;
    Database databases[MAX_DATABASES];
    int num_databases;
} User;

User users[10]; // Array of users
int num_users = 0;

int authenticate_user(const char *username, const char *password) {
    for (int i = 0; i < num_users; i++) {
        if (strcmp(username, users[i].username) == 0 && strcmp(password, users[i].password) == 0) {
            return i; // Return the index of the authenticated user
        }
    }
    return -1; // Authentication failed
}

int get_database_index(const char *database_name, int user_index) {
    for (int i = 0; i < users[user_index].num_databases; i++) {
        if (strcmp(database_name, users[user_index].databases[i].name) == 0) {
            return i; // Return the index of the database
        }
    }
    return -1; // Database not found
}

int has_permission(const char *database_name, int user_index) {
    if (users[user_index].root) {
        return 1; // Root user has permission for all databases
    }

    int database_index = get_database_index(database_name, user_index);
    if (database_index >= 0 && users[user_index].databases[database_index].permission) {
        return 1; // User has permission for the database
    }

    return 0; // User does not have permission for the database
}

void grant_permission(const char *database_name, int user_index) {
    int database_index = get_database_index(database_name, user_index);
    if (database_index >= 0) {
        users[user_index].databases[database_index].permission = 1;
        printf("Permission granted for database %s to user %s\n", database_name, users[user_index].username);
    } else {
        printf("Database %s not found\n", database_name);
    }
}

void create_database(const char *database_name, int user_index) {
    if (users[user_index].num_databases >= MAX_DATABASES) {
        printf("Maximum number of databases reached for user %s\n", users[user_index].username);
        return;
    }

    for (int i = 0; i < users[user_index].num_databases; i++) {
        if (strcmp(database_name, users[user_index].databases[i].name) == 0) {
            printf("Database %s already exists for user %s\n", database_name, users[user_index].username);
            return;
        }
    }

    strcpy(users[user_index].databases[users[user_index].num_databases].name, database_name);
    users[user_index].databases[users[user_index].num_databases].permission = 1;
    users[user_index].num_databases++;
    printf("Database %s created for user %s\n", database_name, users[user_index].username);
}

void handle_client(int client_socket) {
    char username[256];
    char password[256];
    int user_index;

    // Receive username from client
    memset(username, 0, sizeof(username));
    read(client_socket, username, sizeof(username));

    // Receive password from client
    memset(password, 0, sizeof(password));
    read(client_socket, password, sizeof(password));

    // Authenticate the user
    user_index = authenticate_user(username, password);

    if (user_index >= 0) {
        // Authentication successful
        printf("User %s authenticated\n", users[user_index].username);

        // Send authentication success message to the client
        send(client_socket, "Authentication successful.", strlen("Authentication successful."), 0);

        // Handle authenticated client requests
        while (1) {
            char command[256];
            memset(command, 0, sizeof(command));
            read(client_socket, command, sizeof(command));

            if (strcmp(command, "CREATE DATABASE") == 0) {
                char database_name[256];
                memset(database_name, 0, sizeof(database_name));
                read(client_socket, database_name, sizeof(database_name));
                create_database(database_name, user_index);
            } else if (strcmp(command, "GRANT PERMISSION") == 0) {
                char database_name[256];
                memset(database_name, 0, sizeof(database_name));
                read(client_socket, database_name, sizeof(database_name));
                grant_permission(database_name, user_index);
            } else if (strcmp(command, "USE DATABASE") == 0) {
                char database_name[256];
                memset(database_name, 0, sizeof(database_name));
                read(client_socket, database_name, sizeof(database_name));

                if (has_permission(database_name, user_index)) {
                    printf("User %s accessed database %s\n", users[user_index].username, database_name);

                    // Send success message to the client
                    send(client_socket, "Database accessed.", strlen("Database accessed."), 0);

                    // Handle database-specific commands here
                    // Implement your logic for handling tables, columns, and queries
                    
                    // Example:
                    // read client commands
                    // execute queries on the specified database
                    // send results back to the client
                } else {
                    printf("User %s does not have permission to access database %s\n", users[user_index].username, database_name);

                    // Send error message to the client
                    send(client_socket, "Permission denied.", strlen("Permission denied."), 0);
                }
            } else if (strcmp(command, "EXIT") == 0) {
                printf("User %s disconnected\n", users[user_index].username);
                break;
            } else {
                // Invalid command
                send(client_socket, "Invalid command.", strlen("Invalid command."), 0);
            }
        }
    } else {
        // Authentication failed
        send(client_socket, "Authentication failed.", strlen("Authentication failed."), 0);
    }

    close(client_socket);
}

int main() {
    // Add sample users for authentication
    strcpy(users[0].username, "root");
    strcpy(users[0].password, "password");
    users[0].root = 1;
    users[0].num_databases = 0;

    strcpy(users[1].username, "user1");
    strcpy(users[1].password, "pass1");
    users[1].root = 0;
    users[1].num_databases = 0;

    num_users = 2;

    int server_fd, client_socket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    // Create socket file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Set socket options
    int opt = 1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to given IP and port
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // Start listening for incoming connections
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("Server is running as a daemon...\n");

    // Accept client connections and handle them
    while (1) {
        if ((client_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        if (fork() == 0) {
            // Child process
            close(server_fd);
            handle_client(client_socket);
            exit(0);
        } else {
            // Parent process
            close(client_socket);
        }
    }

    return 0;
}